<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class KontakSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kontak')->insert([
            [
                'nama_kontak' => 'Telepon',
                'kontak'      => '0271-593068',
            ],
            [
                'nama_kontak' => 'Email',
                'kontak'      => 'baznaskab.sukoharjo@baznas.go.id',
            ],
            [
                'nama_kontak' => 'Alamat',
                'kontak'      => 'Jl. Jend. Sudirman No. 199 Sukoharjo',
            ],
        ]);
    }
}
