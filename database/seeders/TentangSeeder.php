<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TentangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tentang')->insert([[
            'profil' => '<div>Berdasarkan Surat Keputusan Bupati Sukoharjo Nomor 451.12/265 tanggal 5 April 2022 tentang penetapan pimpinan Badan Amil Zakat Nasional Kabupaten Sukoharjo periode 2022-2027. Ditetapkan Pimpinan BAZNAS Kabupaten Sukoharjo periode 2022-2027 sebagai berikut:<br>1. Ketua : Sardiyono<br>2. Wakil Ketua I : Sofwan Faisal Sifyan<br>3. Wakil Ketua II : Legiman Luqman Hakim<br>4. Wakil Ketua III : Joko Purwanto<br>5. Wakil Ketua IV : Wahyono.</div>',
            'gambarprofil' => 'ketua.jpeg',
            'visi' => '<div>Badan Amil Zakat Nasional (BAZNAS) Kabupaten Sukoharjo menjadi pengelola zakat yang profesional, amanah, transparan dan akuntabel, serta ikut berperan aktif dalam menanggulangi kemiskinan dan peningkatan kesejahteraan, keberdayaan umat islam di Kabupaten Sukoharjo.</div>',
            'misi' => '<div>1. Meningkatkan pemahaman dan kesadaran umat islam khususnya Aparatur Sipil Negara (ASN) untuk menunaikan zakatnya melalui Baznas Kabupaten Sukoharjo.<br>2. Mengelola Zakat Infak dan Sedekah (ZIS) secara profesional, terstandarisasi berbasis IT, sehingga menjadi lembaga yang akuntabel.<br>3. Menyalurkan dan mendayagunakan zakat secara optimal sesuai ketentuan syariat islam dalam menanggulangi kemiskinan dan meningkatkan kesejahteraan dan keberdayaan mustahik.<br>4. Memperkuat jaringan dengan Organisasi Pengelola Zakat (OPZ), Pemerintah Kabupaten, dan stakeholder terkait lainnya.</div>',
            'gambarvimi' => 'ketua.jpeg',
            'struktur' => '',
        ]]);
    }
}
