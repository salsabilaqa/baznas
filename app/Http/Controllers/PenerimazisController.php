<?php
namespace App\Http\Controllers;

use App\Models\Penerimazis;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class PenerimazisController extends Controller
{
    public function index(Request $request)
    {
        if ($request->tahun) {
                $zis = Penerimazis::with('user')
                ->where(DB::raw('tahun'), $request->tahun)->get();
        } else {
                $zis = Penerimazis::with('user')
                ->where(DB::raw('tahun'), now())->get();
        }
    
    // $tahun = DB::table('penerimazis')->select(DB::raw('tahun'))->orderBy(DB::raw('tahun'), 'desc')->groupBy(DB::raw('tahun'))->pluck('tahun');

    $data = [
        // 'tahun' => $tahun,
        'penerima' => $zis
        ];

        return view('laporan_zis.laporanzis', $data);
    }

    public function create()
    {
        return view('laporan_zis.create-zis');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bulan' => 'required',
            'tahun' => 'required',
            'zakat' => 'required|integer',
            'infaq_terikat' => 'required|integer',
            'infaq_umum' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }
        $zakat = $request->zakat;
        $infaq_terikat = $request->infaq_terikat;
        $infaq_umum = $request->infaq_umum;
        $total = $zakat + $infaq_terikat + $infaq_umum;

        Penerimazis::create([
            'bulan' => $request->bulan,
            'tahun' => $request->tahun,
            'zakat' => $zakat,
            'infaq_terikat' => $infaq_terikat,
            'infaq_umum' => $infaq_umum,
            'total' => $total,
            'user_id' => $request->user,
        ]);

        // dd($request);
        return redirect('/laporan_zis')->with('success', 'Laporan Penerima ZIS baru berhasil dibuat!');
    }

    public function show($id)
    {
        $data = [
            'zis' => Penerimazis::find($id),
        ];
        return view('laporan_zis.detail', $data);
    }

    public function edit($id)
    {
        return view('laporan_zis.edit-zis', [
            'zis' => Penerimazis::find($id)
        ]);
    }
    
    public function update(Request $request, $id)
    {
        $rules = [
            'bulan' => 'required',
            'tahun' => 'required',
            'zakat' => 'required|integer',
            'infaq_terikat' => 'required|integer',
            'infaq_umum' => 'required|integer'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('toast_error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        $zakat = $request->zakat;
        $infaq_terikat = $request->infaq_terikat;
        $infaq_umum = $request->infaq_umum;
        $total = $zakat + $infaq_terikat + $infaq_umum;

        Penerimazis::where('id', $id)
            ->update([
                'bulan' => $request->bulan,
                'tahun' => $request->tahun,
                'zakat' => $zakat,
                'infaq_terikat' => $infaq_terikat,
                'infaq_umum' => $infaq_umum,
                'total' => $total,
                'user_id' => $request->user,
            ]);

        return redirect('/laporan_zis')->with('success', 'Laporan Penerima ZIS berhasil diubah!');
    }

    public function delete($id)
    {
        Penerimazis::destroy($id);
        return redirect('/laporan_zis')->with('success', 'Laporan Penerima ZIS berhasil dihapus!');
    }

}
