@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <a href="/galeri">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#6c757d"
                                    class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16">
                                    <path
                                        d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z" />
                                </svg>
                            </a>
                            <p class="card-description mx-2">Kembali</p>
                        </div>
                        <h4 class="card-title text-center">Form Edit Galeri</h4>
                        <form action="{{ url('/galeri/update/' . $galeri->id) }}" method="post" class="forms-sample"
                            enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <label for="judul">Nama Album</label>
                                <input type="text"
                                    class="form-control form-control-sm @error('judul') is-invalid @enderror" id="judul"
                                    name="judul" placeholder="Nama Album" value="{{ old('judul', $galeri->judul) }}">
                                @error('judul')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="tanggal">Tanggal</label>
                                <input type="date"
                                    class="form-control form-control-sm @error('tanggal') is-invalid @enderror"
                                    id="tanggal" name="tanggal" placeholder="Tanggal"
                                    value="{{ old('tanggal', $galeri->tanggal) }}">
                                @error('tanggal')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="deskripsi">Deskripsi</label>
                                <textarea name="deskripsi" class="form-control" id="" rows="10"
                                    placeholder="Tuliskan deskripsi atau caption">{{ old('tanggal', $galeri->deskripsi) }}</textarea>
                                @error('deskripsi')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Upload Foto</label>
                                <div id="preview-container">
                                    @foreach ($galeri->image as $img)
                                        @if ($galeri->image)
                                            <img src="{{ asset('admin/images/galeri/' . $img->foto) }}" id="preview"
                                                height="100" class="preview-image mr-2 mb-3" title="{{ $img->foto }}">
                                        @endif
                                        <input type="hidden" name="oldImage[]" value="{{ $img->foto }}">
                                    @endforeach
                                </div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input @error('foto') is-invalid @enderror"
                                        name="foto[]" id="foto" multiple onchange="previewMultipleImages()">
                                    <label class="custom-file-label text-secondary" for="foto">
                                        @foreach ($galeri->image as $img)
                                            {{ $img->foto ?? 'Choose file' }}
                                        @endforeach
                                    </label>
                                </div>
                                @error('foto')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button type="reset" class="btn btn-inverse-danger btn-fw">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function previewMultipleImages() {

            const input = document.getElementById('foto');
            const previewContainer = document.getElementById('preview-container');
            const files = input.files;

            if (files.length === 0) {
                return;
            }

            // Loop through selected files
            for (let i = 0; i < files.length; i++) {
                const file = files[i];

                // Create new FileReader
                const reader = new FileReader();

                // Set preview image src
                reader.onload = () => {
                    const previewImage = document.createElement('img');
                    previewImage.classList.add('preview-image');
                    previewImage.src = reader.result;
                    previewContainer.appendChild(previewImage);
                };

                // Read the file as a data URL
                reader.readAsDataURL(file);
            }

            let preview = document.querySelectorAll('img');
            preview.forEach((preview) => {
                preview.style.display = "none";
            });
            document.getElementById("preview").style.display = "none";
        }
    </script>
@endsection
