@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin">
                <div class="row">
                    <div class="col-12 col-xl-8 mb-4 mb-xl-0">
                        <h3 class="font-weight-bold">Pengaturan Kalkulator Zakat</h3>
                        {{-- <h6 class="font-weight-normal mb-0">All systems are running smoothly! You have <span class="text-primary">3 unread alerts!</span></h6> --}}
                    </div>
                </div>
            </div>
        </div>
        @foreach ($emas as $em)
        <div class="row">
            <div class="col-md-6 grid-margin transparent">
                <div class="row">
                    <div class="col-md-12 mb-4 stretch-card transparent">
                        <div class="card card-tale">
                            <div class="card-body">
                                <p class="mb-4">Harga Emas</p>
                                <p class="fs-30 mb-2">Rp {{ number_format($em->harga, 0, ',', '.') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mb-4 stretch-card transparent">
                        <div class="card card-light-danger">
                            <div class="card-body">
                                <p class="mb-4">Update Harga Emas</p>
                                <form action="{{ url('/pengaturan/kalkulator/hargaemas/' . $em->id) }}" method="post" class="row">
                                    @csrf
                                    @method('put')
                                    <div class="col-lg-8">
                                        <input type="number"
                                            class="form-control form-control-sm @error('harga') is-invalid @enderror"
                                            id="harga" name="harga" placeholder="Masukan Harga Emas Terbaru" value="{{ old('harga') }}">
                                        @error('harga')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-lg-4">
                                        <button type="submit" class="btn btn-primary mr-2">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 stretch-card transparent">
                <div class="card" style="background: url('{{ asset('admin/images/emas.jpeg') }}')">
                    <div class="card-body text-light mt-5">
                        <h2 class="mb-4 font-weight-bold">Harga Emas</h2>
                        <p class="mb-3">Harga emas dalam perhitungan zakat berfungsi sebagai penentu nilai nishab (batas
                            minimum) harta yang harus dikeluarkan zakatnya.</p>
                        <p>Nishab zakat maal dan zakat penghasilan adalah 85 gram emas, yang artinya seseorang harus
                            membayar zakat jika harta yang dimilikinya telah mencapai atau melebihi nilai 85 gram emas.</p>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection
