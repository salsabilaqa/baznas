@extends('layouts.landingpage')

@section('content')

<section class="section-padding section-bg" id="section_2">
    <div class="container">
        <div class="row">

            <div class="col-lg-6 col-12 mb-5 mb-lg-0">
                <img src="{!! asset('admin/images/tentang/' . $tentang[0]->gambarprofil)!!}" class="custom-text-box-image img-fluid" alt="">
            </div>

            <div class="col-lg-6 col-12">
                <div class="custom-text-box">
                    <h2 class="mb-2">Profile Baznas Sukoharjo</h2>
                    <p class="mb-0">{!! $tentang[0]->profil !!}</p>
                </div>
            </div>

        </div>
    </div>
</section>

@endsection