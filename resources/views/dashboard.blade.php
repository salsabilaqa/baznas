@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin">
                <div class="row">
                    <div class="col-12 col-xl-8 mb-4 mb-xl-0">
                        <h3 class="font-weight-bold">Selamat Datang, {{ Auth::user()->name }}!</h3>
                        {{-- <h6 class="font-weight-normal mb-0">All systems are running smoothly! You have <span class="text-primary">3 unread alerts!</span></h6> --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 grid-margin stretch-card">
                <div class="card" style="background: gainsboro">
                    <div class="card-people mt-auto">
                        <h3 class="mb-2 text-center">Penerima ZIS</h3>
                        <h5 class="mb-2 text-center">{{ $tahun }}</h5>
                        <canvas id="myChart" class="mx-2 my-2"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-4 grid-margin transparent">
                <div class="row">
                    <div class="col-md-12 mb-4 stretch-card transparent"></a>
                        <div class="card card-tale">
                            <div class="card-body">
                                <h4 class="mb-2">Jumlah Artikel</h4>
                                <h6 class="mb-4">{{ $tahun}}</h6>
                                <p class="fs-30 mb-2">{{ $artikel->count()}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mb-4 stretch-card transparent">
                        <div class="card card-dark-blue">
                            <div class="card-body">
                                <h4 class="mb-4">Harga Emas</h4>
                                <p class="fs-30 mb-2">@currency($emas->harga)</p>
                                <br>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col-md-6 stretch-card transparent">
                        <div class="card card-light-danger">
                            <div class="card-body">
                                <p class="mb-4">Number of Clients</p>
                                <p class="fs-30 mb-2">47033</p>
                                <p>0.22% (30 days)</p>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>

      <!-- begin::ChartJS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
    <script>
        var ctx = document.getElementById('myChart');
        var chart = new Chart(ctx, {
            type: 'bar',
            data:{
                labels:{!! json_encode($bulan) !!},
                datasets: [{
                    label: 'Zakat',
                    data: {!! json_encode($zakat) !!},
                    backgroundColor: 'rgba(255, 248, 0, 1.0)',
                    borderColor: 'rgba(255, 248, 0, 1.0)',
                    borderWidth: 1
                },
                {
                    label: 'Infaq Terikat',
                    data: {!! json_encode($infaq_terikat) !!},
                    backgroundColor: 'rgba(32, 11, 170, 1.0)',
                    borderColor: 'rgba(32, 11, 170, 1.0)',
                    borderWidth: 1
                },
                {
                    label: 'Infaq Umum',
                    data: {!! json_encode($infaq_umum) !!},
                    backgroundColor: 'rgba(124, 207, 0, 1.0)',
                    borderColor: 'rgba(124, 207, 0, 1.0)',
                    borderWidth: 1
                },
                {
                    label: 'Total',
                    data: {!! json_encode($total) !!},
                    backgroundColor: 'rgba(160, 5, 45, 1.0)',
                    borderColor: 'rgba(160, 5, 45, 1.0)',
                    borderWidth: 1
                },
            ]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
@endsection
