@extends('layouts.landingpage')

@section('content')
    <section class="news-detail-header-section text-center">
        <div class="section-overlay"></div>

        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-12">
                    <h1 class="text-white">Artikel dan Berita</h1>
                </div>

            </div>
        </div>
    </section>

    <section class="news-section section-padding">
        <div class="container">
            <div class="row">

                <div class="col-lg-7 col-12">
                    <div class="news-block">
                        <div class="news-block-top">
                            <img src="{!! asset('/admin/images/artikel/' . $artikel->image) !!}" class="news-image img-fluid" alt="">
                        </div>

                        <div class="news-block-info">
                            <div class="d-flex mt-2">
                                <div class="news-block-date">
                                    <p>
                                        <i class="bi-calendar4 custom-icon me-1"></i>
                                        {{ date('d M Y', strtotime($artikel->created_at)) }}
                                    </p>
                                </div>

                                <div class="news-block-author mx-5">
                                    <p>
                                        <i class="bi-person custom-icon me-1"></i>
                                        By {{ $artikel->author->name }}
                                    </p>
                                </div>
                            </div>

                            <div class="news-block-title mb-4">
                                <h4>{{ $artikel->judul }}</h4>
                            </div>

                            <div class="news-block-body">
                                {!! $artikel->body !!}

                                {{-- <blockquote>Sed leo nisl, posuere at molestie ac, suscipit auctor mauris. Etiam quis metus
                                    elementum, tempor risus vel, condimentum orci.</blockquote> --}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-12 mx-auto mt-4 mt-lg-0">
                    <form class="custom-form search-form" action="#" method="post" role="form">
                        <input class="form-control" type="search" id="mySearch" placeholder="Search" aria-label="Search">

                        <button type="submit" class="form-control">
                            <i class="bi-search"></i>
                        </button>
                    </form>

                    <h5 class="mt-5 mb-3">Recent news</h5>
                    <div id="myCard">
                        @foreach ($posts->except($artikel->id) as $post)
                            <div class="news-block news-block-two-col d-flex mt-4" id="col">
                                <div class="news-block-two-col-image-wrap">
                                    <a href="{{ url('/' . $post->slug) }}">
                                        <img src="{!! asset('/admin/images/artikel/' . $post->image) !!}" class="news-image img-fluid" alt="{{ $post->image }}">
                                    </a>
                                </div>

                                <div class="news-block-two-col-info">
                                    <div class="news-block-title mb-2">
                                        <h6><a href="{{ url('/' . $post->slug) }}"
                                                class="news-block-title-link">{{ Str::words($post->judul, 3, '...') }}</a>
                                        </h6>
                                    </div>

                                    <div class="news-block-date">
                                        <p>
                                            <i class="bi-calendar4 custom-icon me-1"></i>
                                            {{ date('d M Y', strtotime($post->created_at)) }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
