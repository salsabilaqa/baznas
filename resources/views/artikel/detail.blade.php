@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="ml-xl-4 mt-3">
                            <div class="d-flex justify-content-between">
                                <p class="card-title">Detail Artikel</p>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="/artikel" type="button" class="btn btn-inverse-secondary btn-fw"><i class="ti-arrow-left"></i></a>
                                    <a href="/artikel/edit/{{ $artikel->slug }}" type="button" class="btn btn-inverse-secondary btn-fw"><i class="ti-pencil-alt"></i></a>
                                    <a href="/artikel/delete/{{ $artikel->slug }}" type="button" class="btn-delete btn btn-inverse-secondary btn-fw"><i class="ti-trash"></i></a>
                                </div>
                            </div>
                            <h3 class="text-primary font-weight-bold mt-3">{{ $artikel->judul }}</h3>
                            <h6 class="font-weight-500 mb-xl-4 text-primary">
                                {{ date('d M Y H:i:s', strtotime($artikel->created_at)) }}</h6>
                            @if ($artikel->image)
                                <div class="card">
                                    <div class="card-people mt-auto">
                                        <img src="{{ asset('admin/images/artikel/' . $artikel->image) }}" style="max-height:350px; width:auto" alt="gambar-artikel">
                                    </div>
                                </div>
                            @endif
                            <p class="mb-2 py-3 mb-xl-0" style="max-height:auto; width:100%">{!! $artikel->body !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
