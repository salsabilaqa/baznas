@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex">
                            <a href="/pengaturan/rekening">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#6c757d"
                                    class="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16">
                                    <path
                                        d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z" />
                                </svg>
                            </a>
                            <p class="card-description mx-2">Kembali</p>
                        </div>
                        <h4 class="card-title text-center">Form Tambah Rekening</h4>
                        <form action="/pengaturan/rekening/store" method="post" class="forms-sample" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="bank">Nama Bank</label>
                                <input type="text"
                                    class="form-control form-control-sm @error('bank') is-invalid @enderror" id="bank"
                                    name="bank" placeholder="Nama Bank" value="{{ old('bank') }}" required>
                                @error('bank')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="no_rekening">No. Rekening</label>
                                <input type="text"
                                    class="form-control form-control-sm @error('no_rekening') is-invalid @enderror" id="no_rekening"
                                    name="no_rekening" placeholder="No. Rekening" value="{{ old('no_rekening') }}" required>
                                @error('no_rekening')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="nama_rekening">Nama Pemilik Rekening</label>
                                <input type="text"
                                    class="form-control form-control-sm @error('nama_rekening') is-invalid @enderror" id="nama_rekening"
                                    name="nama_rekening" placeholder="Nama Pemilik Rekening" value="{{ old('nama_rekening') }}" required>
                                @error('nama_rekening')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Upload Logo Bank</label>
                                <img class="img-preview img-fluid mb-3 col-sm-5">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input @error('logo') is-invalid @enderror"
                                        name="logo" id="image" onchange="previewImage()" required>
                                    <label class="custom-file-label text-secondary" for="logo">Choose file</label>
                                </div>
                                @error('logo')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button type="reset" class="btn btn-inverse-danger btn-fw">Reset</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <script>
            /* Mengoptimalkan fungsi choose file */
            var fileInput = document.getElementsByClassName("custom-file-input");
    
            for (var i = 0; i < fileInput.length; i++) {
                var input = fileInput[i];
                input.addEventListener('change', function(e) {
                    var files = [];
                    for (var i = 0; i < $(this)[0].files.length; i++) {
                        files.push($(this)[0].files[i].name);
                    }
                    $(this).next('.custom-file-label').html(files.join(', '));
                })
            }
        </script>
@endsection

