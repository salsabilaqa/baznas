@extends('layouts.landingpage')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h3 class="py-3 text-center">Rekening Baznas Sukoharjo</h3>
                        <div class="table-responsive mb-3">
                            <table class="table table-striped" id="myTable" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="10%">No</th>
                                        <th class="text-center" width="20%">Nama Bank</th>
                                        <th class="text-center" width="30%">No. Rekening</th>
                                        <th class="text-center" width="40%">Nama Pemilik Rekening</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($rekening as $rek)
                                    <tr>
                                        <td class="text-center">{{ $loop->iteration }}</td>
                                        <td class="text-center"> 
                                            <img src="{{ asset('admin/images/rekening/' . $rek->logo) }}" class="img-thumbnail" alt="gambar-logo-bank">
                                        </td>
                                        <td class="text-center">{{ $rek->no_rekening }}</td>
                                        <td class="text-center">{{ $rek->nama_rekening }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection