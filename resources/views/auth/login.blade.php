<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Baznas Sukoharjo</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{!! asset('admin/vendors/feather/feather.css') !!}">
    <link rel="stylesheet" href="{!! asset('admin/vendors/ti-icons/css/themify-icons.css') !!}">
    <link rel="stylesheet" href="{!! asset('admin/vendors/css/vendor.bundle.base.css') !!}">
    <!-- endinject -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{!! asset('admin/css/vertical-layout-light/style.css') !!}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{!! asset('images/logo3.png') !!}" />
</head>

<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center auth px-0">
                <div class="row w-100 mx-0">
                    <div class="col-lg-4 mx-auto">
                        <div class="auth-form-light rounded text-left py-5 px-4 px-sm-5">
                            <div class="brand-logo d-flex justify-content-center mb-4">
                                {{-- <img src="{!! asset('images/Kabupaten_Sukoharjo.png') !!}" class="w-25" alt="logo"> --}}
                                <img src="{!! asset('images/logo.png') !!}" class="w-30" alt="logo">
                            </div>
                            <h4>Selamat Datang!</h4>
                            <h6 class="font-weight-light">Sign in untuk melanjutkan.</h6>
                            <form class="pt-3" method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group">
                                    <input type="email" id="email" type="email"
                                        class="form-control @error('email') is-invalid @enderror" name="email"
                                        value="{{ old('email') }}" required autocomplete="email" autofocus
                                        placeholder="Email Address">
                                    @error('email')
                                        <span class="invalid-feedback mt-2" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                                        name="password" required autocomplete="current-password" placeholder="Password">
                                    @error('password')
                                        <span class="invalid-feedback mt-2" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="mt-3">
                                    <button type="submit" class="btn btn-block btn-primary font-weight-medium auth-form-btn">SIGN IN</button>
                                </div>
                                {{-- <div class="my-2 d-flex justify-content-between align-items-center">
                  <div class="form-check">
                    <label class="form-check-label text-muted">
                      <input type="checkbox" class="form-check-input">
                      Keep me signed in
                    </label>
                  </div>
                  <a href="#" class="auth-link text-black">Forgot password?</a>
                </div>
                <div class="mb-2">
                  <button type="button" class="btn btn-block btn-facebook auth-form-btn">
                    <i class="ti-facebook mr-2"></i>Connect using facebook
                  </button>
                </div>
                <div class="text-center mt-4 font-weight-light">
                  Don't have an account? <a href="register.html" class="text-primary">Create</a>
                </div> --}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{!! asset('admin/vendors/js/vendor.bundle.base.js') !!}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{!! asset('admin/js/off-canvas.js') !!}"></script>
    <script src="{!! asset('admin/js/hoverable-collapse.js') !!}"></script>
    <script src="{!! asset('admin/js/template.js') !!}"></script>
    <script src="{!! asset('admin/js/settings.js') !!}"></script>
    <script src="{!! asset('admin/js/todolist.js') !!}"></script>
    <!-- endinject -->
</body>

</html>