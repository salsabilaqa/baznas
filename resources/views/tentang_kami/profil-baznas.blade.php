@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="ml-xl-4 mt-3">
                            <div class="d-flex justify-content-between">
                                <h4 class="card-title">Profil Baznas</h4>
                                <a href="{{ url('/pengaturan/edit-profil/' . $tentang[0]->id) }}"><button type="button" class="btn btn-inverse-info btn-fw">
                                        <i class="ti-pencil-alt"></i> Edit
                                    </button></a>
                            </div>
                            <form class="form-sample mt-3" action=""
                                method="post">
                                <div class="form-group row">
                                    <label class="col-sm-2" for="gambarprofil">Gambar</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" name="oldImage" value="{{ $tentang[0]->gambarprofil }}">
                                        @if ($tentang[0]->gambarprofil)
                                            <img src="{{ asset('admin/images/tentang/' . $tentang[0]->gambarprofil) }}"
                                                class="img-preview img-fluid img-thumbnail mb-3 col-sm-5" id="gambar-preview">
                                        @else
                                            <img class="img-preview img-fluid mb-3 col-sm-5">
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2" for="profil">Profil</label>
                                    <div class="col-sm-10">
                                        <div class="card px-3 py-3" style="background: gainsboro">
                                            <p>{!! $tentang[0]->profil !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="ml-xl-4 mt-3">
                            <div class="d-flex justify-content-between">
                                <h4 class="card-title">Visi Misi</h4>
                                <a href="{{ url('/pengaturan/edit-vimi/' . $tentang[0]->id) }}"><button type="button" class="btn btn-inverse-info btn-fw">
                                        <i class="ti-pencil-alt"></i> Edit
                                    </button></a>
                            </div>
                            <form class="form-sample mt-3" action=""
                                method="post">
                                <div class="form-group row">
                                    <label class="col-sm-2" for="gambarvimi">Gambar</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" name="oldImage" value="{{ $tentang[0]->gambarvimi }}">
                                        @if ($tentang[0]->gambarvimi)
                                            <img src="{{ asset('admin/images/tentang/' . $tentang[0]->gambarvimi) }}"
                                                class="img-preview img-fluid img-thumbnail mb-3 col-sm-5" id="gambar-preview">
                                        @else
                                            <img class="img-preview img-fluid mb-3 col-sm-5">
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2" for="visi">Visi</label>
                                    <div class="col-sm-10">
                                        <div class="card px-3 py-3" style="background: gainsboro">
                                            <p>{!! $tentang[0]->visi !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2" for="misi">Misi</label>
                                    <div class="col-sm-10">
                                        <div class="card px-3 py-3" style="background: gainsboro">
                                            <p>{!! $tentang[0]->misi !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
