<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item {{ Request::is('dashboard') ? 'active' : '' }}">
            <a class="nav-link" href="/dashboard">
                <i class="icon-grid menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('artikel*') ? 'active' : '' }}">
            <a class="nav-link" href="/artikel">
                <i class="icon-paper menu-icon"></i>
                <span class="menu-title">Artikel</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('galeri*') ? 'active' : '' }}">
            <a class="nav-link" href="/galeri">
                <i class="ti-gallery menu-icon"></i>
                <span class="menu-title">Galeri</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('laporan_zis*') ? 'active' : '' }}">
            <a class="nav-link" href="/laporan_zis">
                <i class="icon-layout menu-icon"></i>
                <span class="menu-title">Laporan Penerima ZIS</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('pengaturan*') ? 'active' : '' }}">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <i class="ti-settings menu-icon"></i>
                <span class="menu-title">Pengaturan</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item {{ Request::is('pengaturan/tentangkami*') ? 'active' : '' }}"><a class="nav-link" href="/pengaturan/tentangkami">Profil, Visi dan Misi</a></li>
                    <li class="nav-item {{ Request::is('pengaturan/kalkulator*') ? 'active' : '' }}"><a class="nav-link" href="/pengaturan/kalkulator">Kalkulator Zakat</a></li>
                    <li class="nav-item {{ Request::is('pengaturan/rekening*') ? 'active' : '' }}"><a class="nav-link" href="/pengaturan/rekening">Rekening</a></li>
                    <li class="nav-item {{ Request::is('pengaturan/sosmed*') ? 'active' : '' }}"><a class="nav-link" href="/pengaturan/sosmed">Sosial Media</a></li>
                    <li class="nav-item {{ Request::is('pengaturan/kontak*') ? 'active' : '' }}"><a class="nav-link" href="/pengaturan/kontak">Kontak</a></li>
                </ul>
            </div>
        </li>
    </ul>
</nav>
