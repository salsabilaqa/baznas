<section class="volunteer-section section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-11">
                <h2 class="text-white mb-4 d-flex">Laporan Penerima ZIS</h2>

                <form class="custom-form volunteer-form mb-5 mb-lg-0" action="{{ url('/') }}" method="get"
                    role="form" id="myForm">
                    <div class="row d-flex justify-content-center mb-3">
                        <div class="form-group d-flex col-lg-4">
                            <label class="px-2 mt-2" for="">Tahun</label>
                            <select name="tahun" class="form-select" id="mySelect">
                                <?php
                                $thn_skr = date('Y');
                                for ($x = $thn_skr; $x >= 2015; $x--) {
                                ?>
                                <option value="<?php echo $x; ?>" {{ request()->tahun == $x ? 'selected' : '' }}>
                                    <?php echo $x; ?></option>
                                <?php
                            }
                            ?>
                            </select>
                        </div>
                    </div>
                    <canvas id="myChart"></canvas>
                </form>
            </div>
        </div>
    </div>
</section>

<script>
    document.getElementById('mySelect').addEventListener('change', function() {
        this.form.submit();
    });
</script>
<!-- begin::ChartJS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
<script>
    // fungsi untuk format rupiah
    const formatRupiah = (value) => {
        let result = value.toString().split('').reverse().join('').match(/\d{1,3}/g);
        result = result.join('.').split('').reverse().join('');
        return 'Rp ' + result;
    };

    var ctx = document.getElementById('myChart');
    var chart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: {!! json_encode($bulan) !!},
            datasets: [{
                    label: 'Zakat',
                    data: {!! json_encode($zakat) !!},
                    backgroundColor: 'rgba(255, 248, 0, 1.0)',
                    borderColor: 'rgba(255, 248, 0, 1.0)',
                    borderWidth: 1
                },
                {
                    label: 'Infaq Terikat',
                    data: {!! json_encode($infaq_terikat) !!},
                    backgroundColor: 'rgba(32, 11, 170, 1.0)',
                    borderColor: 'rgba(32, 11, 170, 1.0)',
                    borderWidth: 1
                },
                {
                    label: 'Infaq Umum',
                    data: {!! json_encode($infaq_umum) !!},
                    backgroundColor: 'rgba(124, 207, 0, 1.0)',
                    borderColor: 'rgba(124, 207, 0, 1.0)',
                    borderWidth: 1
                },
                {
                    label: 'Total',
                    data: {!! json_encode($total) !!},
                    backgroundColor: 'rgba(160, 5, 45, 1.0)',
                    borderColor: 'rgba(160, 5, 45, 1.0)',
                    borderWidth: 1
                },
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        callback: function(value, index, values) {
                            return formatRupiah(value);
                        }
                    }
                }]
            }
        }
    });
</script>
