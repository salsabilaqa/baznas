<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">

    <title>BAZNAS Kabupaten Sukoharjo</title>

    <!-- CSS FILES -->
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/bootstrap-icons.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/templatemo-kind-heart-charity.css') !!}">
    <link rel="shortcut icon" href="{!! asset('images/logo3.png') !!}" />
    <style>
        .attachment a img {
            width: 100%;
            height: auto;
        }

        .whatsapp {
            position: fixed;
            right: 20px;
            bottom: 20px;
            width: 60px;
            height: 60px;
            background-image: url('/images/whatsapp.png');
            background-repeat: no-repeat;
            background-size: contain;
            border-radius: 50%;
            text-align: center;
            font-size: 30px;
            color: #fff;
            z-index: 1000;
        }
    </style>
</head>

<body>

    <header class="site-header">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 col-12 d-flex flex-wrap">
                    <p class="d-flex me-4 mb-0">
                        <i class="bi-geo-alt me-2"></i>
                        {{ $alamat }}
                    </p>

                    <p class="d-flex mb-0">
                        <i class="bi-envelope me-2"></i>

                        <a href="mailto:{{ $email }}">
                            {{ $email }}
                        </a>
                    </p>
                </div>

                <div class="col-lg-3 col-12 ms-auto d-lg-block d-none">
                    <ul class="social-icon">
                        <li class="social-icon-item">
                            <a href="{{ $twitter }}" class="social-icon-link bi-twitter" target="_blank"></a>
                        </li>

                        <li class="social-icon-item">
                            <a href="{{ $facebook }}" class="social-icon-link bi-facebook" target="_blank"></a>
                        </li>

                        <li class="social-icon-item">
                            <a href="{{ $instagram }}" class="social-icon-link bi-instagram" target="_blank"></a>
                        </li>

                        <li class="social-icon-item">
                            <a href="{{ $youtube }}" class="social-icon-link bi-youtube" target="_blank"></a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </header>

    <nav class="navbar navbar-expand-lg bg-light shadow-lg">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="{!! asset('images/logo.png') !!}" class="logo img-fluid" alt="Kind Heart Charity">
                <span style="font-size: 14pt">
                    Baznas Sukoharjo
                </span>
            </a>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item">
                        <a class="nav-link {{ request()->routeIs('home') ? 'active' : '' }}" href="/">Beranda</a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link {{ Request::is('tentang_kami*') ? 'active' : '' }} dropdown-toggle"
                            id="navbarLightDropdownMenuLink" role="button" data-bs-toggle="dropdown"
                            aria-expanded="false">Tentang</a>

                        <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarLightDropdownMenuLink">
                            <li><a class="dropdown-item {{ Request::is('tentang_kami/profil') ? 'active' : '' }}"
                                    href="/tentang_kami/profil">Profil</a></li>
                            <li><a class="dropdown-item {{ Request::is('tentang_kami/visi') ? 'active' : '' }}"
                                    href="/tentang_kami/visi">Visi dan Misi</a></li>
                            {{-- <li><a class="dropdown-item {{ Request::is('tentang_kami/struktur') ? 'active' : '' }}" href="/tentang_kami/struktur">Struktur</a></li> --}}
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{ request()->routeIs('allposts') || request()->routeIs('detailpost') ? 'active' : '' }}"
                            href="/posts">Artikel</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('landing-galeri') ? 'active' : '' }}" href="/landing-galeri">Galeri</a>
                    </li>
                    {{-- <li class="nav-item">
                            <a class="nav-link {{ Request::is('dashboard') ? 'active' : '' }}" href="#section_3">Artikel</a>
                        </li> --}}

                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('penerima_zis/zis') ? 'active' : '' }}"
                            href="/penerima_zis/zis">Penerima ZIS</a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link {{ Request::is('kalkulator*') ? 'active' : '' }} dropdown-toggle"
                            id="navbarLightDropdownMenuLink" role="button" data-bs-toggle="dropdown"
                            aria-expanded="false">Kalkulator</a>

                        <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarLightDropdownMenuLink">
                            <li><a class="dropdown-item {{ Request::is('kalkulator/zakat_penghasilan') ? 'active' : '' }}"
                                    href="/kalkulator/zakat_penghasilan">Zakat Penghasilan</a></li>

                            <li><a class="dropdown-item {{ Request::is('kalkulator/zakat_maal') ? 'active' : '' }}"
                                    href="/kalkulator/zakat_maal">Zakat Maal</a></li>
                        </ul>
                    </li>

                    <li class="nav-item ms-3">
                        <a class="nav-link custom-btn custom-border-btn btn {{ Request::is('rekening/baznas') ? 'active' : '' }}" href="/rekening/baznas">Rekening</a>
                    </li>
                </ul>
            </div>
        </div>
        <a href="{{ $whatsapp }}" target="_blank" class="whatsapp"></a>
    </nav>

    <main>

        @yield('content')
    </main>

    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-12 mb-4">
                    <img src="{!! asset('images/logo3.png') !!}" class="logo img-fluid w-25" alt="">
                </div>

                {{-- <div class="col-lg-4 col-md-6 col-12 mb-4">
                        <h5 class="site-footer-title mb-3">Quick Links</h5>
        
                        <ul class="footer-menu">
                            <li class="footer-menu-item"><a href="#" class="footer-menu-link">Our Story</a></li>
        
                            <li class="footer-menu-item"><a href="#" class="footer-menu-link">Newsroom</a></li>
        
                            <li class="footer-menu-item"><a href="#" class="footer-menu-link">Causes</a></li>
        
                            <li class="footer-menu-item"><a href="#" class="footer-menu-link">Become a volunteer</a></li>
        
                            <li class="footer-menu-item"><a href="#" class="footer-menu-link">Partner with us</a></li>
                        </ul>
                    </div> --}}

                <div class="col-lg-4 col-md-6 col-12 mx-auto">
                    <h5 class="site-footer-title mb-3">Contact Infomation</h5>

                    <p class="text-white d-flex mb-2">
                        <i class="bi-telephone me-2"></i>

                        <a href="" class="site-footer-link">
                            {{ $telpon }}
                        </a>
                    </p>

                    <p class="text-white d-flex">
                        <i class="bi-envelope me-2"></i>

                        <a href="mailto:{{ $email }}" class="site-footer-link">
                            {{ $email }}
                        </a>
                    </p>

                    <p class="text-white d-flex mt-3">
                        <i class="bi-geo-alt me-2"></i>
                        {{ $alamat }}
                    </p>
                </div>
            </div>
        </div>

        <div class="site-footer-bottom">
            <div class="container">
                <div class="row">

                    <div class="col-lg-6 col-md-7 col-12">
                        <p class="copyright-text mb-0">Copyright © 2023 <a href="#">Baznas Sukoharjo</a></p>
                    </div>

                    <div class="col-lg-6 col-md-5 col-12 d-flex justify-content-center align-items-center mx-auto">
                        <ul class="social-icon">
                            <li class="social-icon-item">
                                <a href="{{ $twitter }}" class="social-icon-link bi-twitter" target="_blank"></a>
                            </li>

                            <li class="social-icon-item">
                                <a href="{{ $facebook }}" class="social-icon-link bi-facebook" target="_blank"></a>
                            </li>

                            <li class="social-icon-item">
                                <a href="{{ $instagram }}" class="social-icon-link bi-instagram" target="_blank"></a>
                            </li>

                            <li class="social-icon-item">
                                <a href="{{ $youtube }}" class="social-icon-link bi-youtube" target="_blank"></a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </footer>

    <!-- JAVASCRIPT FILES -->
    <script src="{!! asset('js/jquery.min.js') !!}"></script>
    <script src="{!! asset('js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('js/jquery.sticky.js') !!}"></script>
    <script src="{!! asset('js/click-scroll.js') !!}"></script>
    <script src="{!! asset('js/counter.js') !!}"></script>
    <script src="{!! asset('js/custom.js') !!}"></script>
    <script src="{!! asset('js/zakat.js') !!}"></script>

    <!-- Start Search Card-->
    <script>
        function search() {
            // ambil nilai dari elemen input
            var input = document.getElementById("mySearch").value.toLowerCase();

            // ambil elemen div yang akan dicari isinya
            var div = document.getElementById("myCard");

            // ambil semua elemen dalam div tersebut
            var elements = div.getElementsByTagName("div");

            // loop setiap elemen dan cek apakah teks pada elemen tersebut cocok dengan kata kunci pencarian
            for (var i = 0; i < elements.length; i++) {
                if (elements[i].innerText.toLowerCase().indexOf(input) > -1) {
                    elements[i].style.display = "block";
                } else {
                    elements[i].style.display = "none";
                }
            }
        }

        document.getElementById("mySearch").addEventListener("keyup", function() {
            search();
        });
    </script>
</body>

</html>
