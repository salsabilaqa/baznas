@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="ml-xl-4 mt-3">
                            <div class="d-flex justify-content-between">
                                <p class="card-title">Detail Laporan ZIS</p>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="/laporan_zis" type="button" class="btn btn-inverse-secondary btn-fw"><i class="ti-arrow-left"></i></a>
                                    <a href="/laporan_zis/edit/{{ $zis->id }}" type="button" class="btn btn-inverse-secondary btn-fw"><i class="ti-pencil-alt"></i></a>
                                    <a href="/laporan_zis/delete/{{ $zis->id }}" type="button" class="btn-delete btn btn-inverse-secondary btn-fw"><i class="ti-trash"></i></a>
                                  </div>
                            </div>
                            <h2 class="text-primary font-weight-bold py-4 d-flex justify-content-center"> {{ $zis->bulan }} {{ $zis->tahun }}</h2>
                            {{-- <h4 class="text-primary font-weight-bold mt-3"> </h4> --}}
                            <div class="detail">
                                <div class="form-group row">
                                    <label class="col-sm-2" for="judul">Zakat</label>
                                    <div class="col-sm-10">
                                        <input type="text"
                                            class="form-control form-control-sm @error('judul') is-invalid @enderror"
                                            id="judul" name="judul" placeholder="Nama Album"
                                            value="@currency($zis->zakat)" readonly>
                                        @error('judul')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2" for="judul">Infaq Terikat</label>
                                    <div class="col-sm-10">
                                        <input type="text"
                                            class="form-control form-control-sm @error('judul') is-invalid @enderror"
                                            id="judul" name="judul" placeholder="Nama Album"
                                            value="@currency($zis->infaq_terikat)" readonly>
                                        @error('judul')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">  
                                    <label class="col-sm-2" for="judul">Infaq Umum</label>
                                    <div class="col-sm-10">
                                        <input type="text"
                                            class="form-control form-control-sm @error('judul') is-invalid @enderror"
                                            id="judul" name="judul" placeholder="Nama Album"
                                            value="@currency($zis->infaq_umum)" readonly>
                                        @error('judul')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2" for="judul">Total</label>
                                    <div class="col-sm-10">
                                        <input type="text"
                                            class="form-control form-control-sm @error('judul') is-invalid @enderror"
                                            id="judul" name="judul" placeholder="Nama Album"
                                            value="@currency($zis->total)" readonly>
                                        @error('judul')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
