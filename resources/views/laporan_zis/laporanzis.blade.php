@extends('layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <h4 class="card-title">Laporan Penerima ZIS</h4>
                                <a href="/laporan_zis/create"><button type="button" class="btn btn-success btn-sm">
                                    + Tambah
                                </button></a>
                        </div>
                        <div class="d-flex justify-content-between mt-3">
                            <form action="{{ url('/laporan_zis') }}" method="GET">
                                @csrf
                                <div class="form-group d-flex">
                                    <label class="px-2 mt-2">Tahun:</label>
                                    <select class="form-control form-control-sm" name="tahun">
                                        <?php
                                        $thn_skr = date('Y');
                                        for ($x = $thn_skr; $x >= 2015; $x--) {
                                        ?>
                                        <option value="<?php echo $x ?>" {{ request()->tahun == $x ? 'selected' : '' }}><?php echo $x ?></option>
                                    <?php
                                    }
                                    ?>
                                    </select>
                                    <button type="submit" class="btn btn-sm btn-secondary text-white"
                                        style="margin-left: 5px;">Cari</button>
                                </div>
                            </form>
                            <div class="col-lg-6">
                                <div class="form-group d-flex">
                                    <label class="px-2 mt-2">Cari:</label>
                                    <input type="text" class="form-control form-control-sm" id="searchInput" placeholder="Cari di sini...">
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive mb-3">
                            <table class="table table-striped" id="myTable" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th class="text-center">Bulan</th>
                                        <th class="text-center">Tahun</th>
                                        <th class="text-center">Zakat</th>
                                        <th class="text-center">Infaq Terikat</th>
                                        <th class="text-center">Infaq Umum</th>
                                        <th class="text-center">Total</th>
                                        <th class="text-center">Pembuat</th>
                                        <th class="text-center" class="text-center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($penerima as $zis)
                                        <tr>
                                            <td class="text-center">{{ $loop->iteration }}</td>
                                            <td class="text-center">{{ $zis->bulan }}</td>
                                            <td class="text-center">{{ $zis->tahun }}</td>
                                            <td class="text-center">@currency($zis->zakat)</td>
                                            <td class="text-center">@currency($zis->infaq_terikat)</td>
                                            <td class="text-center">@currency($zis->infaq_umum)</td>
                                            <td class="text-center">@currency($zis->total)</td>
                                            <td class="text-center">{{ $zis->user->name }}</td>
                                            <td class="d-flex justify-content-center">
                                                <a href="/laporan_zis/detail/{{ $zis->id }}"><button class="btn btn-inverse-primary btn-icon mx-1"><i class="ti-eye"></i></button></a>
                                                <a href="/laporan_zis/edit/{{ $zis->id }}"><button class="btn btn-inverse-info btn-icon mx-1"><i class="ti-pencil"></i></button></a>
                                                <form class="btn-delete" action="/laporan_zis/delete/{{ $zis->id }}" method="post">
                                                    @method('delete')
                                                    @csrf
                                                    <button type="submit" class="btn btn-inverse-danger btn-icon">
                                                        <i class="ti-trash"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

