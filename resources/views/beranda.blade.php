@extends('layouts.landingpage')

@section('content')
    <section class="hero-section hero-section-full-height">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-12 p-0">
                    <div id="hero-slide" class="carousel carousel-fade slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="{!! asset('admin/images/artikel/' . $posts[0]->image) !!}" class="carousel-image img-fluid" alt="...">

                                <div class="carousel-caption d-flex flex-column justify-content-end" style="opacity: 75%">
                                    <h2>{{ Str::words($posts[0]->judul, 2, '') }}</h2>
                                    <h5>{{ Str::words(Str::after($posts[0]->judul, Str::words($posts[0]->judul, 2, '')), 5, '') }}
                                    </h5>
                                    {{-- <h5>{{ Str::after($posts[0]->judul, Str::words($posts[0]->judul, 2, '')) }}</h5> --}}
                                    {{-- <p>{{ Str::words(strip_tags($art->body), 7, '...') }}</p> --}}
                                </div>
                            </div>

                            @foreach ($posts->take(3)->skip(1) as $art)
                                <div class="carousel-item">
                                    <img src="{!! asset('admin/images/artikel/' . $art->image) !!}" class="carousel-image img-fluid" alt="...">

                                    <div class="carousel-caption d-flex flex-column justify-content-end"
                                        style="opacity: 75%">
                                        <h2>{{ Str::words($art->judul, 2, '') }}</h2>

                                        <h5>{{ Str::words(Str::after($art->judul, Str::words($art->judul, 2, '')), 5, '') }}
                                        </h5>
                                    </div>
                                </div>
                            @endforeach

                            {{-- <div class="carousel-item">
                            <img src="{!! asset('images/slide/medium-shot-people-collecting-donations.jpg')!!}" class="carousel-image img-fluid" alt="...">
                            
                            <div class="carousel-caption d-flex flex-column justify-content-end">
                                <h1>Humanity</h1>
                                
                                <p>Please tell your friends about our website</p>
                            </div>
                        </div> --}}
                        </div>

                        <button class="carousel-control-prev" type="button" data-bs-target="#hero-slide"
                            data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>

                        <button class="carousel-control-next" type="button" data-bs-target="#hero-slide"
                            data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="news-section section-padding">
        <div class="container">
            <div class="row">

                <div class="col-lg-7 col-12">
                    @foreach ($posts->take(2) as $post)
                        <div class="news-block mb-4">
                            <div class="news-block-top">
                                <a href="{{ url('/' . $post->slug) }}">
                                    <img src="{!! asset('/admin/images/artikel/' . $post->image) !!}" class="news-image img-fluid" alt="">
                                </a>
                            </div>

                            <div class="news-block-info">
                                <div class="d-flex mt-2">
                                    <div class="news-block-date">
                                        <p>
                                            <i class="bi-calendar4 custom-icon me-1"></i>
                                            {{ date('d M Y', strtotime($post->created_at)) }}
                                        </p>
                                    </div>

                                    <div class="news-block-author mx-5">
                                        <p>
                                            <i class="bi-person custom-icon me-1"></i>
                                            By {{ $post->author->name }}
                                        </p>
                                    </div>
                                </div>

                                <div class="news-block-title mb-2">
                                    <h4><a href="{{ url('/' . $post->slug) }}"
                                            class="news-block-title-link">{{ $post->judul }}</a>
                                    </h4>
                                </div>

                                <div class="news-block-body">
                                    <p>{{ Str::words(strip_tags($post->body), 30, '...') }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="col-lg-4 col-12 mx-auto mt-4 mt-lg-0">
                    <form class="custom-form search-form" action="#" method="post" role="form">
                        <input class="form-control" type="search" id="mySearch" placeholder="Search" aria-label="Search">

                        <button type="submit" class="form-control">
                            <i class="bi-search"></i>
                        </button>
                    </form>

                    <h5 class="mt-5 mb-3">Recent news</h5>

                    <div id="myCard">
                        @foreach ($posts->skip(2) as $post)
                            <div class="news-block news-block-two-col d-flex mt-4" id="col">
                                <div class="news-block-two-col-image-wrap">
                                    <a href="{{ url('/' . $post->slug) }}">
                                        <img src="{!! asset('/admin/images/artikel/' . $post->image) !!}" class="news-image img-fluid h-100"
                                            alt="">
                                    </a>
                                </div>

                                <div class="news-block-two-col-info">
                                    <div class="card-body news-block-title mb-2">
                                        <h6><a href="{{ url('/' . $post->slug) }}"
                                                class="news-block-title-link">{{ Str::words($post->judul, 3, '...') }}</a>
                                        </h6>
                                    </div>

                                    <div class="news-block-date">
                                        <p>
                                            <i class="bi-calendar4 custom-icon me-1"></i>
                                            {{ date('d M Y', strtotime($post->created_at)) }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

   @include('layouts/grafik')
@endsection
